#!/bin/bash




################## CONFIG ##############
# Minimum 20x20
#WIDTH=20
#HEIGHT=20
WIDTH=60
HEIGHT=45
# Full Terminal
#WIDTH=$(( $(tput cols) / 2 - 1 ))
#HEIGHT=$(( $(tput lines) - 2 ))

# Start length
LENGTH=3

# Start direction (bit compilicated)
DIRX=1
DIRY=0
INVALIDKEYS="ws"


SOLIDWALL=0
################## CONFIG ##############









# Initializing some variables
SNAKE=""
GAMEOVER=0

# not actual default start value
APPLEX=$HEADX
APPLEY=$HEADY


function drawstartscreen() {

	#		37
	#+------------------------------------+
	#|                                    |
	#|        > Press any button <        |
	#|                                    |
	#|                         w          |
	#|        Movement       a s d        |
	#|                                    | 18
	#|        Pause            p          |
	#|                                    |
	#|        Quit             q          |
	#|                                    |
	#|        Redraw           r          |
	#|                                    |
	#|        Boardsize       x/c         |
	#|                                    |
	#|         BashSnake by Jake          |
	#|                                    |
	#+------------------------------------+
	
	SSX=$(( $HEIGHT / 2 - 9 + $OFFX ))
	SSY=$(( $WIDTH - 18 + $OFFY ))
	
	tput setab 0
	tput setaf 4

	tput cup $SSX $SSY
	echo -n "+------------------------------------+"
	for x in $(seq 1 16); do
		tput cup $(( $SSX + $x )) $SSY
		echo -n "|                                    |"
	done
	tput cup $(( $SSX + 17 )) $SSY
	echo -n "+------------------------------------+"


	tput cup $(( $SSX + 2 )) $(( $SSY + 1 ))
	tput setaf 3
	echo -n "        > Press any button <        "
	tput cup $(( $SSX + 4 )) $(( $SSY + 1 ))
	tput setaf 2
	echo -n "                         w          "
	tput cup $(( $SSX + 5 )) $(( $SSY + 1 ))
	echo -n "        $(tput setaf 7)Movement       $(tput setaf 2)a s d        "
	tput cup $(( $SSX + 7 )) $(( $SSY + 1 ))
	echo -n "        $(tput setaf 7)Pause            $(tput setaf 2)p        "
	tput cup $(( $SSX + 9 )) $(( $SSY + 1 ))
	echo -n "        $(tput setaf 7)Quit             $(tput setaf 2)q        "
	tput cup $(( $SSX + 11 )) $(( $SSY + 1 ))
	echo -n "        $(tput setaf 7)Redraw           $(tput setaf 2)r        "
	tput cup $(( $SSX + 13 )) $(( $SSY + 1 ))
	echo -n "        $(tput setaf 7)Boardsize       $(tput setaf 2)x/c        "
	tput cup $(( $SSX + 15 )) $(( $SSY + 1 ))
	echo -n "         $(tput setaf 2)BashSna$(tput setaf 3)ke $(tput setaf 1)by $(tput setaf 5)Jake          "

}


function drawpoint() {
	tput cup $(( $OFFX + ${1} )) $(( ${2} * 2 + $OFFY ))
	tput setaf $3
	tput setab $3
	echo -ne "${4}${4}"
}

function drawoutline() {
	tput clear
	tput setaf 4
	tput setab 0
	tput civis
	#-- invisible
	#tput cnorm   -- normal
	tput cup $OFFX $OFFY
	printf "+%*s+" $(( $WIDTH *2 -1 )) |tr " " "-"
	for x in $(seq 1 $(( $HEIGHT -1 )) ); do
		tput cup $(( $OFFX + $x ))  $OFFY
		#echo "x: $x"	
		printf "|%*s|" $(( $WIDTH * 2 -1 ))
	done
	tput cup $(( $OFFX + $HEIGHT ))  $OFFY
	printf "+%*s+" $(( $WIDTH *2 -1 )) |tr " " "-"

}

function drawfullsnake() {
	echo -e "$SNAKE" | grep ',' | while IFS="," read CURX CURY; do
		[ "$CURX" != "" ] && drawpoint $CURX $CURY 2 "#"
		#echo "x: $CURX y: $CURY"
	done
	drawpoint $HEADX $HEADY 3 "O"
	#tput setaf 7
}

function drawapple() {
	drawpoint $APPLEX $APPLEY 1 "\$"
}

function drawscore() {
	tput cup $OFFX $(( $WIDTH - 11 + $OFFY ))
	tput setab 0
	tput setaf 6
	printf ' >>> Length: %3.3d <<< ' "$LENGTH"
}

function drawall() {
	calculateoffset
	drawoutline
	drawfullsnake
	drawapple
	drawscore
}





function intersectionwithsnake() {
	#echo "$SNAKE" | grep  "^${1},${2}\$" | wc -l
	echo "$SNAKE" | sed 's,^,A,;s,$,B,'| grep -F "A${1},${2}B" | wc -l
}


function randomapple() {
	NAPPLEX=$APPLEX
	NAPPLEY=$APPLEY
	#until [ "$(intersectionwithsnake $NAPPLEX $NAPPLEY)" = "0" ]; do 
	#until [ "$(intersectionwithsnake $NAPPLEX $NAPPLEY)" = "0" ] && [ $NAPPLEX != $APPLEX ] && [ $NAPPLEY != $APPLEY ]; do 
	while [ "$(intersectionwithsnake $NAPPLEX $NAPPLEY)" != "0" ] || [ "$NAPPLEX" = "$APPLEX" ] || [ "$NAPPLEY" = "$APPLEY" ]; do 
		NAPPLEX=$(( $RANDOM % ( $HEIGHT -1 ) + 1 ))
		NAPPLEY=$(( $RANDOM % ( $WIDTH -1 ) + 1 ))
	done
	APPLEX=$NAPPLEX
	APPLEY=$NAPPLEY
}





function movesnake() {
	#echo "headx: $HEADX"
	#echo "headY: $HEADY"
	#echo "snake: $SNAKE"
	PREVHEADX=$HEADX
	PREVHEADY=$HEADY
	HEADX=$(( $HEADX + $DIRX ))
	HEADY=$(( $HEADY + $DIRY ))

	if [ "$SOLIDWALL" = "1" ]; then
		[ $HEADX -ge $HEIGHT ] && GAMEOVER=1
		[ $HEADY -ge $WIDTH ] &&  GAMEOVER=1
		[ $HEADX -le 0 ] &&       GAMEOVER=1
		[ $HEADY -le 0 ] &&       GAMEOVER=1
	else

		[ $HEADX -ge $HEIGHT ] && HEADX=1
		[ $HEADY -ge $WIDTH ] && HEADY=1
		[ $HEADX -le 0 ] && HEADX=$(( $HEIGHT -1 ))
		[ $HEADY -le 0 ] && HEADY=$(( $WIDTH -1  ))

	fi

	drawpoint $HEADX $HEADY 3 "O"
	drawpoint $PREVHEADX $PREVHEADY 2 "#"
	drawpoint $(echo -en "$SNAKE" | tail -n 1 | cut -d ',' -f 1) $(echo -en "$SNAKE" | tail -n 1 | cut -d ',' -f 2)  0 " "
	SNAKE="$(echo -e "$HEADX,$HEADY\n$SNAKE")"

	if [ "$(intersectionwithsnake $APPLEX $APPLEY)" = "0" ]; then
		SNAKE="$(echo -e "$SNAKE" | head -n -1)"
	else
		randomapple
		drawapple
		LENGTH="$(( $LENGTH + 1 ))"
		drawscore
	fi
	if [ "$(intersectionwithsnake $HEADX $HEADY)" != "1" ]; then
		GAMEOVER=1
	fi
	#tput setaf 5
	#echo -e "\rintersect: $(intersectionwithsnake $HEADX $HEADY)"
}



function pausegame() {
	CENTERX=$(( $HEIGHT / 2 ))
	CENTERY=$(( $WIDTH / 2 ))
	
	tput setab 0
	tput setaf 3
	tput cup $(( $CENTERX + $OFFX )) $(( $CENTERY * 2 - 8 + $OFFY ))
	echo ">>> Paused! <<<"

	read -N 1 -s

	drawall
}

function updateresolution() {
	OLDHEIGHT="$HEIGHT"
	OLDWIDTH="$WIDTH"

	case "${1}" in
		'i') WIDTH=$(( $WIDTH + 1 )) ; HEIGHT=$(( $HEIGHT + 1 )) ;;
		'd') WIDTH=$(( $WIDTH - 1 )) ; HEIGHT=$(( $HEIGHT - 1 )) ;;
	esac
	if [ "$HEIGHT" -lt 20 ] || [ "$WIDTH" -lt 20 ] || [ "$WIDTH" -gt "$(( $(tput cols) / 2 - 1 ))" ] || [ "$HEIGHT" -gt "$(( $(tput lines) - 2 ))" ]; then
		HEIGHT="$OLDHEIGHT"
		WIDTH="$OLDWIDTH"
	else

		[ $APPLEX -ge $HEIGHT ] && APPLEX=1
		[ $APPLEY -ge $WIDTH ] && APPLEY=1
		[ $APPLEX -le 0 ] && APPLEX=$(( $HEIGHT -1 ))
		[ $APPLEY -le 0 ] && APPLEY=$(( $WIDTH -1  ))

		drawall
	fi
}

function snakeloop() {
	while [ $GAMEOVER = 0 ]; do
		#echo new cycle
		read -s -N 1 -r -t 0.001 KEY
		#echo loop \"$KEY\"
		if [ "$(echo "$INVALIDKEYS" | grep -F "$KEY")" = "" ]; then
			case "$KEY" in
				'w') DIRX="-1" ; DIRY="0"  ;INVALIDKEYS="ws" ;;
				'a') DIRX="0"  ; DIRY="-1" ;INVALIDKEYS="da" ;;
				's') DIRX="1"  ; DIRY="0"  ;INVALIDKEYS="ws" ;;
				'd') DIRX="0"  ; DIRY="1"  ;INVALIDKEYS="ad" ;;
				'r') drawall ;;
				'p') pausegame ;;
				'q') GAMEOVER=1 ;;
				'c') updateresolution 'i' ;;
				'x') updateresolution 'd' ;;
			esac
		fi
		movesnake
		#echo -en "\r x: $DIRX y: $DIRY Snake: \"$(echo "$SNAKE" | tr '\n' ' ')\""
		#sleep 0.04
	done

}

function initsnake() {
	for i in $(seq 0 $(( $LENGTH - 1 )) ); do
		SNAKE="$(( $HEADX - ( $DIRX * $i ) )),$(( $HEADY - ( $DIRY * $i ) ))\n$SNAKE"
	done

}

function printgameover() {
	
	CENTERX=$(( $HEIGHT / 2 ))
	CENTERY=$(( $WIDTH / 2 ))
	
	tput setab 0
	tput setaf 3
	tput cup $(( $CENTERX -1 + $OFFX )) $(( $CENTERY * 2 - 9 + $OFFY ))
	echo ">>> Game Over! <<<"
	tput cup $(( $CENTERX + $OFFX ))   $(( $CENTERY * 2 - 6 + $OFFY ))
	echo " Length: $LENGTH "
	tput setaf 4
	tput cup $(( $CENTERX + 1 + $OFFX )) $(( $CENTERY * 2 - 9 + $OFFY ))
	echo " Press any Button "
}

function startscreen() {
	drawstartscreen
	read -N 1 -s
	drawall
}

function checkresolution() {
	if [ "$HEIGHT" -lt 20 ] || [ "$WIDTH" -lt 20 ] || [ "$WIDTH" -gt "$(( $(tput cols) / 2 - 1 ))" ] || [ "$HEIGHT" -gt "$(( $(tput lines) - 2 ))" ]; then
		echo "Error: Wrong Resolution: ${WIDTH}x${HEIGHT}   Minimum allowed: 20x20   Current Maximum allowed: $(( $(tput cols) / 2 - 1 ))x$(( $(tput lines) - 2 ))" >&2
		exit 1
	fi

}

function calculateoffset() {
	OFFY="$(( $(tput cols) / 2 - 1 - $WIDTH  ))"
	OFFX="$(( ( $(tput lines) - 2 - $HEIGHT ) / 2 ))"
}

function main() {
	# Start position (center)
	HEADX=$(( $HEIGHT / 2 ))
	HEADY=$(( $WIDTH / 2 ))

	checkresolution
	tput smcup
	tput setaf 7
	tput setab 0
	#reset
	stty -echo


	initsnake
	randomapple
	drawall
	startscreen
	snakeloop
		
	printgameover
	read -N 1 -s
		
	stty echo
	tput rmcup
	tput setaf 7
	tput setab 0
	tput sgr0
	echo "Game Over!"
	echo "Resolution: ${WIDTH}x${HEIGHT}"
	echo "Length: $LENGTH"
}

function printhelp() {
	echo "Usage: $1 <WIDTH>[x]<HEIGHT>" >&2
	echo "Minimum allowed: 20x20   Current Maximum allowed: $(( $(tput cols) / 2 - 1 ))x$(( $(tput lines) - 2 ))"
}


#echo "$#"
if [ "$#" = "2" ] && [ "$(echo "$1" | grep '^[0-9][0-9]*$')" != "" ] && [ "$(echo "$2" | grep '^[0-9][0-9]*$')" != "" ]; then 
	WIDTH="$1"
	HEIGHT="$2"
	main
elif [ "$#" = "1" ] && [ "$(echo "$1" | grep '^[0-9][0-9]*x[0-9][0-9]*$')" != "" ]; then 
	WIDTH="$(echo "$1" | cut -d 'x' -f 1)"
	HEIGHT="$(echo "$1" | cut -d 'x' -f 2)"
	main
elif [ "$#" = "0" ]; then
	main
else
	printhelp "$0"
	exit 1
fi

